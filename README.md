SGWRAP
======
Set of [CWRAP](https://cwrap.org/) libraries to run distributed applications with [Remote SimGrid](https://framagit.org/simgrid/remote-simgrid/).

How to run an application with SGWRAP?
--------------------------------------
1. Create a wrapper script for each application, that runs the application by LD_PRELOADing (a subset of) our libraries — as usual with CWRAP.
2. Run a RSG server.
3. Add initial actors to the RSG server — i.e., a series of ``rsg add-actor --no-autoconnect -- <YOUR-WRAPPER> [<YOUR-ARGS>...]``
4. Run the simulation.

Differences w.r.t. original CWRAP
---------------------------------
- socket_wrapper: New functions prefixed by ``sgwrap_``, that are called in place of ``libc_`` functions in ``swrap_`` original functions.
- new wrapper libraries.
