{ kapack ? import
    (fetchTarball "https://github.com/oar-team/kapack/archive/master.tar.gz")
  {}
}:

let
  pkgs = kapack.pkgs;
  packages = rec {
    sgwrap = pkgs.stdenv.mkDerivation rec {
      pname = "sgwrap";
      version = "0.1.0-git";

      src = ./.;

      nativeBuildInputs = with pkgs; [ meson pkgconfig ninja ];
      buildInputs = [ kapack.remote_simgrid_dev ] ++
        [ kapack.simgrid ]; # for tesh

      preConfigure = "rm -rf build";
    };
    socket_wrapper = pkgs.stdenv.mkDerivation rec {
      pname = "sgwrap-socket-wrapper";
      version = "0.1.0-git";

      src = ./socket_wrapper;
      nativeBuildInputs = with pkgs; [ cmake pkgconfig ];
      buildInputs = [ kapack.remote_simgrid_dev ];

      preConfigure = "rm -rf build";
    };
  };
in
  packages
