#include <stdlib.h>
#include <string.h>

#include <librsg.hpp>

static void autoconnect(void) __attribute__((constructor));
void autoconnect(void)
{
  char *initial_value = getenv("SOCKET_WRAPPER_DIR");
  if (initial_value != nullptr)
  {
    char *copy = strdup(initial_value);
    unsetenv("SOCKET_WRAPPER_DIR");

    rsg::connect();

    setenv("SOCKET_WRAPPER_DIR", copy, 1);
    free(copy);
  }
  else
  {
    rsg::connect();
  }
}
