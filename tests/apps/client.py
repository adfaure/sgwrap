#!/usr/bin/env python3
import socket
import sys

server_host = "localhost"
server_port = 2000

s = socket.socket(socket.AF_INET, socket.-SOCK_STREAM)

s.connect((server_host, server_port))
s.send("Hello world".encode('utf-8'))
data = s.recv(1024)
print(data)
