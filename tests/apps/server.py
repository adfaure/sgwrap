#!/usr/bin/env python3
import socket

my_host = ""
my_port = 2000

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((my_host, my_port))
s.listen(5)

while True:
    # wait for next client to connect
    connection, _ = s.accept()
    while True:
        # receive up to 1K bytes
        data = connection.recv(1024).decode('utf-8')
        if data:
            connection.send(f"echo -> {data}".encode('utf-8'))
        else:
            break
    connection.close()
