#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

int main(int argc , char *argv[])
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  printf("initial time: %ld.%06ld\n", tv.tv_sec, tv.tv_usec);

  int sleep_seconds = 2;
  printf("sleeping for %d seconds\n", sleep_seconds);
  sleep(sleep_seconds);
  printf("sleeping done\n");

  gettimeofday(&tv, NULL);
  printf("final time: %ld.%06ld\n", tv.tv_sec, tv.tv_usec);

  return 0;
}
