#include <librsg.hpp>
#include <math.h>

extern "C" {
  unsigned int sleep(unsigned int seconds);
  int gettimeofday(struct timeval *tv, struct timezone *tz);
  time_t time(time_t *t);
}

unsigned int sleep(unsigned int seconds) {
  rsg::this_actor::sleep_for(seconds);
  return 0;
}

int gettimeofday(struct timeval *tv, struct timezone *tz) {
  double clock = rsg::Engine::get_clock();
  tv->tv_sec = floor(clock);
  tv->tv_usec = (clock - tv->tv_sec) * 1000.0 * 1000.0;
  return 0;
}

time_t time(time_t *t) {
  time_t clock = (time_t)(rsg::Engine::get_clock());
  if(t != NULL)
    *t = clock;
  return clock;
}
